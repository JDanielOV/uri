package ContestdeNatal2020;

import java.util.Vector;
import java.util.Scanner;
import java.util.Collections;

public class GnomesTeams {

    public void GnomesTeams() {
        Vector<Gnome> vListGnomes = new Vector();
        Scanner scan = new Scanner(System.in);
        int iSizeList = scan.nextInt();
        for (int i = 0; i < iSizeList; i++) {
            String sName = scan.next();
            int iAge = scan.nextInt();
            vListGnomes.add(new Gnome(sName, iAge));
        }
        Collections.sort(vListGnomes);
        for (int i = 0; i < iSizeList / 3; i++) {
            System.out.println("Time " + (i + 1));
            for (int j = i; j < vListGnomes.size(); j += (iSizeList / 3)) {
                System.out.println(vListGnomes.get(j).getName() + " " + vListGnomes.get(j).getAge());
            }
            System.out.println("");
        }
    }

    static class Gnome implements Comparable<Gnome> {

        String sName;
        int iAge;

        public Gnome(String sName, int iAge) {
            this.sName = sName;
            this.iAge = iAge;
        }

        public String getName() {
            return this.sName;
        }

        public int getAge() {
            return this.iAge;
        }

        public int compareTo(Gnome o) {
            if (iAge < o.iAge) {
                return 1;
            }
            if (o.iAge < iAge) {
                return -1;
            }
            if (sName.compareTo(o.sName) < 0) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
