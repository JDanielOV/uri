package ContestdeNatal2020;

import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

public class NoelsWorkGroups {

    public void NoelsWorkGroups() {
        Map<String, Integer> mListHoursJobs = new HashMap();
        Scanner scan = new Scanner(System.in);
        int iCases = scan.nextInt();
        int iPresentTotal = 0;
        for (int i = 0; i < iCases; i++) {
            String sNameGnomes = scan.next();
            String sAreaJob = scan.next();
            int iHoursJob = scan.nextInt();
            if (mListHoursJobs.get(sAreaJob) == null) {
                mListHoursJobs.put(sAreaJob, iHoursJob);
            } else {
                mListHoursJobs.put(sAreaJob, (mListHoursJobs.get(sAreaJob)) + iHoursJob);
            }
        }
        for (Map.Entry<String, Integer> entry : mListHoursJobs.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            if (key.equals("bonecos")) {
                iPresentTotal += (Math.floor(value / 8));
            } else if (key.equals("arquitetos")) {
                iPresentTotal += (Math.floor(value / 4));
            } else if (key.equals("musicos")) {
                iPresentTotal += (Math.floor(value / 6));
            } else if (key.equals("desenhistas")) {
                iPresentTotal += (Math.floor(value / 12));
            }
        }
        System.out.println(iPresentTotal);
    }
}
