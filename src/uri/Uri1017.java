/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1017 {
    public static void U1017()
    {
        Scanner scan=new Scanner(System.in);
        DecimalFormat formato =new DecimalFormat("#.000");
        double horas,km;
        double res;
        horas=scan.nextInt();
        km=scan.nextInt();
        res=km/12;
        res=res*horas;
        System.out.println(formato.format(res));
        
    }
}
