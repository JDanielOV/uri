/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1094 {
    public static void U1094()
    {
        Scanner scan=new Scanner (System.in);
        DecimalFormat formato=new DecimalFormat("#.00");
        int N,x,c=0,r=0,s=0,total=0;
        float proc=0,pror=0,pros=0,conversor;
        char ani;
        N=scan.nextInt();
        String xx;
        for(int i=0;i<N;i++)
        {
            x=scan.nextInt();
            xx=Integer.toString(x);
            conversor=Float.parseFloat(xx);
            total+=x;
            ani=scan.next().charAt(0);
            if(ani=='C')
            {
                c+=conversor;
            }else if(ani=='R')
            {
                r+=conversor;
            }else if(ani=='S')
            {
                s+=conversor;
            }
        }
        proc=((c)/(total));
        proc=proc*100;
        pror=(r/total);
        pror=pror*100;
        pros=(s/total)*100;
        System.out.println("Total: "+total+" cobaias");
        System.out.println("Total de coelhos: "+c);
        System.out.println("Total de ratos: "+r);
        System.out.println("Total de sapos: "+s);
        System.out.println("Percentual de coelhos: "+((c)/(total)));
        System.out.println("Percentual de ratos: "+formato.format(pror));
        System.out.println("Percentual de sapos: "+formato.format(pros));
        
        
    }
}
