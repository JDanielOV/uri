/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1037 {

    public static void Uri1037_() {
        Scanner sc = new Scanner(System.in);
        float valor;
        valor = sc.nextFloat();
        if (valor >= 0 && valor <= 25) {
            System.out.println("Intervalo [0,25]");
        } else if (valor >= 25 && valor <= 50) {
            System.out.println("Intervalo (25,50]");
        } else if (valor >= 50 && valor <= 75) {
            System.out.println("Intervalo (50,75]");
        } else if (valor >= 75 && valor <= 100) {
            System.out.println("Intervalo (75,100]");
        } else {
            System.out.println("Fora de intervalo");
        }
    }
}
