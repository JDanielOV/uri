/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.util.Scanner;
import java.util.Stack;

public class DiamantesyArena1069 {
//<><<>>

    public void DiamantesyArena1069() {
        Scanner scan = new Scanner(System.in);
        int iCasos = scan.nextInt();
        for (int i = 0; i < iCasos; i++) {
            String sCaso = scan.next();
            Stack<Character> sPila = new Stack();
            int iCounterDiamons = 0;
            for (char c : sCaso.toCharArray()) {
                if (sPila.size() >= 1 && sPila.peek() == '<' && c == '>') {
                    sPila.pop();
                    iCounterDiamons++;
                } else if (c != '.') {
                    sPila.add(c);
                }
            }
            System.out.println(iCounterDiamons);
        }
    }
}
