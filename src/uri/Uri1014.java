/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1014 {
    public static void U1014()
    {
        Scanner scan=new Scanner (System.in);
        DecimalFormat formato=new DecimalFormat("#.000");
        int x=scan.nextInt();
        float y=scan.nextFloat();
        float res=x/y;
        System.out.println(formato.format(res)+" km/l");
        
    }
}
