/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1064 {
    public static void U1064()
    {
        Scanner scan=new Scanner (System.in);
        DecimalFormat formato=new DecimalFormat("#.0");
        float x,prom=0;
        int cont=0;
        for(int i=0;i<6;i++)
        {
            x=scan.nextFloat();
            if(x>0)
            {
                cont++;
                prom=prom+x;
            }
        }
        prom=prom/cont;
        System.out.println(cont+" valores positivos");
        System.out.println(prom);
    }
}
