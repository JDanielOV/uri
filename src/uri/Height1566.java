/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author Daniel Ochoa
 */
public class Height1566 {

    public void Height1566() {
        Scanner scan = new Scanner(System.in);
        int iCasos = scan.nextInt();
        for (int i = 0; i < iCasos; i++) {
            int iDatos = scan.nextInt();
            ArrayList<Integer> aList = new ArrayList();
            for (int j = 0; j < iDatos; j++) {
                aList.add(scan.nextInt());
            }
            Comparator<Integer> cComp = (d1, d2) -> d1 - d2;
            aList.sort(cComp);
            for (Integer integer : aList) {
                System.out.print(integer+" ");
            }
            System.out.println("");
        }
    }
}
