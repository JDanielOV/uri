/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1015 {
    public static void U1015()
    {
        double x1,y1;
        double x2,y2;
        double res;
        Scanner scan=new Scanner (System.in);
        DecimalFormat formato=new DecimalFormat("#.0000");
        
        x1=scan.nextDouble();
        y1=scan.nextDouble();
        x2=scan.nextDouble();
        y2=scan.nextDouble();
        res=Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
        System.out.println(formato.format(res));
    }
}
