package uri;

import java.util.*;

public class Uri1256 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int iCasos = scan.nextInt();
        for (int i = 0; i < iCasos; i++) {
            System.out.println();
            Map<Integer, ArrayList<Integer>> mLista = new HashMap<>();
            int iLongitud = scan.nextInt();
            int iDatos = scan.nextInt();
            for (int j = 0; j < iDatos; j++) {
                int iDato = scan.nextInt();
                int iPosicion=iDato % iLongitud;
                if (mLista.get((iPosicion)) == null) {
                    ArrayList<Integer> aTemporal = new ArrayList<>();
                    aTemporal.add(iDato);
                    mLista.put((iPosicion),aTemporal);
                } else {
                    ArrayList<Integer> aTemporal = mLista.get((iPosicion));
                    aTemporal.add(iDato);
                    mLista.put((iPosicion), aTemporal);
                }
            }
            for (int j = 0; j < iLongitud; j++) {
                StringBuilder sMensaje = new StringBuilder();
                if (mLista.get((j)) == null) {
                    System.out.println(j + " -> \\");
                } else {
                    sMensaje.append(j).append(" ->");
                    for (int k : mLista.get((j))) {
                        sMensaje.append(" ").append(k).append(" ->");
                    }
                    sMensaje.append(" \\");
                    System.out.println(sMensaje);
                }
            }
        }
    }
}
