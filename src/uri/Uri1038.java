/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1038 {

   public static void Uri1038_() {
        int X, Y;
        double res=0;
        Scanner scan = new Scanner(System.in);
        X = scan.nextInt();
        Y=scan.nextInt();  
        DecimalFormat formato=new DecimalFormat("#.00");
        switch(X)
        {
            case 1:
                res=Y*4;
                break;
            case 2:
                res=Y*4.50;
                break;
            case 3:
                res=Y*5;
                break;
            case 4:
                res=Y*2;
                break;
            case 5:
                res=Y*1.50;
                break;
        }
        System.out.print("Total: R$ "+formato.format(res)+"\n");
    }
   public static void Uri1038_2()
   {
         int X, Y;
  float price = 0;
  
  Scanner input = new Scanner(System.in);
  X = input.nextInt();
  Y = input.nextInt();
  if (X == 1) {
   price  = (float) (4.00 * Y);
  }else if (X == 2) {
   price  = (float) (4.50 * Y);
  }else if (X == 3) {
   price  = (float) (5.00 * Y);
  }else if (X == 4) {
   price  = (float) (2.00 * Y);
  }else if (X == 5) {
   price  = (float) (1.50 * Y);
  }
  
  System.out.printf("Total: R$ %.2f\n",price);
   }
}
