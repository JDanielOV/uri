package uri;

import java.util.Scanner;

public class TarjetasColeccionables1028 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int iCasos, iRicardo, iVicente, iPilas;
        iCasos = scan.nextInt();
        for (int i = 0; i < iCasos; i++) {
            iRicardo = scan.nextInt();
            iVicente = scan.nextInt();
            int iMin = Math.min(iRicardo, iVicente);
            if (Math.max(iRicardo, iVicente) % iMin == 0) {
                System.out.println(iMin);
                continue;
            }
            for (int j = (iMin / 2) + 1; j > 0; j--) {
                if (iRicardo % j == 0 && iVicente % j == 0) {
                    System.out.println(j);
                    break;
                }
            }
        }
    }
}
