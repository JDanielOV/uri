/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1036 {
    public static void U1036()
    {
        Scanner scan=new Scanner (System.in);
        DecimalFormat formato=new DecimalFormat("#.00000");
        double a,b,c,res,res2;
        a=scan.nextDouble();
        b=scan.nextDouble();
        c=scan.nextDouble();
        if((2*a)==0)
        {
            System.out.println("Impossivel calcular");
            
        }else if(((b*b)-(4*a*c))<0)
        {
            System.out.println("Impossivel calcular");
        }else
        {
            res= ((-1*b)+Math.sqrt((b*b)-(4*a*c)))/(2*a);
            res2=((-1*b)-Math.sqrt((b*b)-(4*a*c)))/(2*a);
            System.out.println("R1 = "+formato.format(res));
            System.out.println("R2 = "+formato.format(res2));
        }
        
    }
}
