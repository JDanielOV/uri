/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1047 {

    public static void Uri1047_() {
        int hi, mi, hf, mf;
        Scanner sc = new Scanner(System.in);
        hi = sc.nextInt();
        mi = sc.nextInt();
        hf = sc.nextInt();
        mf = sc.nextInt();
        int rh, rm;
        rh = hf - hi;
        if (rh == 0) {
            rh = 24;
        } else if (0 > rh) {
            rh = 24 + rh;
        }
        rm = mf - mi;
        if (0 > rm) {
            rm = 60 + rm;
            rh--;
        }
        
        if (hf == hi && mf == mi) {
            System.out.printf("O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)\n");
        } else {
            System.out.println("O JOGO DUROU " + rh + " HORA(S) E " + rm + " MINUTO(S)");
        }
        

    }

    public static void Uri1047_2() {
        int start_h, end_h, start_m, end_m, duration_h, duration_m;

        Scanner sc = new Scanner(System.in);

        start_h = sc.nextInt();
        start_m = sc.nextInt();
        end_h = sc.nextInt();
        end_m = sc.nextInt();

        duration_h = end_h - start_h;

        if (duration_h < 0) {
            duration_h = 24 + (end_h - start_h);
        }

        duration_m = end_m - start_m;
        if (duration_m < 0) {
            duration_m = 60 + (end_m - start_m);
            duration_h--;
        }

        if (start_h == end_h && start_m == end_m) {
            System.out.printf("O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)\n");
        } else {
            System.out.printf("O JOGO DUROU %d HORA(S) E %d MINUTO(S)\n", duration_h, duration_m);
        }

    }
}
