/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Comparator;

/**
 *
 * @author Daniel Ochoa
 */
public class GnomesTeams3176 {

    public void GnomesTeams3176() {
        Scanner scan = new Scanner(System.in);
        int iGnomes = scan.nextInt();
        ArrayList<Gnome> aList = new ArrayList();
        for (int i = 0; i < iGnomes; i++) {
            aList.add(new Gnome(scan.next(), scan.nextInt()));
        }
        Comparator<Gnome> sorterAge = (g1, g2) -> g2.getiAge() - g1.getiAge();
        Comparator<Gnome> sorterName = (g1, g2) -> g1.getsName().compareTo(g2.getsName());
        aList.sort(sorterAge.thenComparing(sorterName));
        for (int i = 0; i < iGnomes / 3; i++) {
            System.out.println("Time "+(i+1));
            for (int j = i; j < iGnomes; j += (iGnomes / 3)) {
                System.out.println(aList.get(j).getsName() + " " + aList.get(j).getiAge());
            }
            System.out.println("");
        }
    }

    public class Gnome {

        private String sName;
        private int iAge;

        public Gnome(String sName, int iAge) {
            this.sName = sName;
            this.iAge = iAge;
        }

        public String getsName() {
            return sName;
        }

        public int getiAge() {
            return iAge;
        }

    }
}
