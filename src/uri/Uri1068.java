package uri;

import java.util.Scanner;

public class Uri1068 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        while (scan.hasNext()) {
            String sProblema = scan.nextLine();
            StringBuilder sPila = new StringBuilder("");
            boolean bControl = true;
            for (int i = 0; i < sProblema.length(); i++) {
                char cValor = sProblema.charAt(i);
                if (cValor == '(' || cValor == ')')
                    if (sPila.length() == 0 && cValor == ')') {
                        sPila.append(cValor);
                        break;
                    } else if (sPila.length() > 0 && sPila.charAt(sPila.length() - 1) == '(' && cValor == ')') {
                        sPila.deleteCharAt(sPila.length() - 1);
                    } else {
                        sPila.append(cValor);
                    }
            }
            if (sPila.length() == 0) {
                System.out.println("correct");
            } else {
                System.out.println("incorrect");
            }
        }

    }
}
