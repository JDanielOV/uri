/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uri;

import java.text.DecimalFormat;
import java.util.Scanner;

/**
 *
 * @author Daniel Ochoa
 */
public class Uri1012 {
    public static void Uri1012_()
    {
        Scanner scan=new Scanner(System.in);
        DecimalFormat formato=new DecimalFormat("#.000");
        double x,y,z;
        x=scan.nextDouble();
        y=scan.nextDouble();
        z=scan.nextDouble();
        
        System.out.println("TRIANGULO: "+formato.format(((x*z)/2)));
        System.out.println("CIRCULO: "+formato.format(((z*z)*3.14159)));
        System.out.println("TRAPEZIO: "+formato.format(((x+y)/2)*z));
        System.out.println("QUADRADO: \n"+formato.format((y*y)));
        System.out.println("RETANGULO: "+formato.format(x*y));
        /**Corregido*/
        
          double a, b, c;

  Scanner sc = new Scanner(System.in);
  a = sc.nextDouble();
  b = sc.nextDouble();
  c = sc.nextDouble();

  System.out.printf("TRIANGULO: %.3f\n", (a * c) / 2);
  System.out.printf("CIRCULO: %.3f\n", c * c * 3.14159);
  System.out.printf("TRAPEZIO: %.3f\n", ((a + b) / 2) * c);
  System.out.printf("QUADRADO: %.3f\n", b * b);
  System.out.printf("RETANGULO: %.3f\n", a * b);

        
        
    }
}
